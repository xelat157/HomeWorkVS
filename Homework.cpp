﻿#include <iostream>

using namespace std;

class Animal {
public:
    virtual void Voice() {
       cout << "Animal voice\n";
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        cout << "Woof!\n";
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        cout << "Meow!\n";
    }
};

class Duck : public Animal {
public:
    void Voice() override {
        cout << "Quack!\n";
    }
};

int main() {
    const int n = 3;
    Animal* animals[n];

    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Duck();

    for (int i = 0; i < n; i++) {
        animals[i]->Voice();
    }

    for (int i = 0; i < n; i++) {
        delete animals[i];
        animals[i] = nullptr;
    }

    return 0;
}
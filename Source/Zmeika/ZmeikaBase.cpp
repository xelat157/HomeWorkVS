// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmeikaBase.h"
#include "ZmeikaElementBase.h"

// Sets default values
AZmeikaBase::AZmeikaBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP;

}

// Called when the game starts or when spawned
void AZmeikaBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddZmeikaElement(5);
	
	
}

// Called every frame
void AZmeikaBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void AZmeikaBase::AddZmeikaElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(ZmeikaElements.Num() * ElementSize, 0, 0);
		FTransform NewTronsform(NewLocation);
		AZmeikaElementBase* NewZmeikaElem = GetWorld()->SpawnActor<AZmeikaElementBase>(ZmeikaElementClass, NewTronsform);
		int32 ElemIndex = ZmeikaElements.Add(NewZmeikaElem);
		if (ElemIndex == 0)
		{
			NewZmeikaElem->SetFirstElementType();
		}
	}
	
}

void AZmeikaBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	MovementSpeed = ElementSize;
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeed;
		break;
	}

	//AddActorWorldOffset(MovementVector);

	for (int i = ZmeikaElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = ZmeikaElements[i];
		auto PrevElement = ZmeikaElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	ZmeikaElements[0]->AddActorWorldOffset(MovementVector);
}


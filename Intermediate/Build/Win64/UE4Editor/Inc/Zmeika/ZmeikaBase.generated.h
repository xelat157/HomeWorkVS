// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZMEIKA_ZmeikaBase_generated_h
#error "ZmeikaBase.generated.h already included, missing '#pragma once' in ZmeikaBase.h"
#endif
#define ZMEIKA_ZmeikaBase_generated_h

#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_SPARSE_DATA
#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_RPC_WRAPPERS
#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZmeikaBase(); \
	friend struct Z_Construct_UClass_AZmeikaBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Zmeika"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaBase)


#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_INCLASS \
private: \
	static void StaticRegisterNativesAZmeikaBase(); \
	friend struct Z_Construct_UClass_AZmeikaBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Zmeika"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaBase)


#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmeikaBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmeikaBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaBase(AZmeikaBase&&); \
	NO_API AZmeikaBase(const AZmeikaBase&); \
public:


#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaBase(AZmeikaBase&&); \
	NO_API AZmeikaBase(const AZmeikaBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AZmeikaBase)


#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_PRIVATE_PROPERTY_OFFSET
#define Zmeika_Source_Zmeika_ZmeikaBase_h_20_PROLOG
#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_PRIVATE_PROPERTY_OFFSET \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_SPARSE_DATA \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_RPC_WRAPPERS \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_INCLASS \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Zmeika_Source_Zmeika_ZmeikaBase_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_PRIVATE_PROPERTY_OFFSET \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_SPARSE_DATA \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_INCLASS_NO_PURE_DECLS \
	Zmeika_Source_Zmeika_ZmeikaBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMEIKA_API UClass* StaticClass<class AZmeikaBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Zmeika_Source_Zmeika_ZmeikaBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> ZMEIKA_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZMEIKA_ZmeikaElementBase_generated_h
#error "ZmeikaElementBase.generated.h already included, missing '#pragma once' in ZmeikaElementBase.h"
#endif
#define ZMEIKA_ZmeikaElementBase_generated_h

#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_SPARSE_DATA
#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_RPC_WRAPPERS \
	virtual void SetFirstElementType_Implementation(); \
 \
	DECLARE_FUNCTION(execSetFirstElementType);


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetFirstElementType);


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_EVENT_PARMS
#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_CALLBACK_WRAPPERS
#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZmeikaElementBase(); \
	friend struct Z_Construct_UClass_AZmeikaElementBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaElementBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Zmeika"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaElementBase)


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAZmeikaElementBase(); \
	friend struct Z_Construct_UClass_AZmeikaElementBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaElementBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Zmeika"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaElementBase)


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmeikaElementBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmeikaElementBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaElementBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaElementBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaElementBase(AZmeikaElementBase&&); \
	NO_API AZmeikaElementBase(const AZmeikaElementBase&); \
public:


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaElementBase(AZmeikaElementBase&&); \
	NO_API AZmeikaElementBase(const AZmeikaElementBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaElementBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaElementBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AZmeikaElementBase)


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_PRIVATE_PROPERTY_OFFSET
#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_11_PROLOG \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_EVENT_PARMS


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_SPARSE_DATA \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_RPC_WRAPPERS \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_CALLBACK_WRAPPERS \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_INCLASS \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_SPARSE_DATA \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_CALLBACK_WRAPPERS \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_INCLASS_NO_PURE_DECLS \
	Zmeika_Source_Zmeika_ZmeikaElementBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMEIKA_API UClass* StaticClass<class AZmeikaElementBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Zmeika_Source_Zmeika_ZmeikaElementBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZMEIKA_ZmeikaGameModeBase_generated_h
#error "ZmeikaGameModeBase.generated.h already included, missing '#pragma once' in ZmeikaGameModeBase.h"
#endif
#define ZMEIKA_ZmeikaGameModeBase_generated_h

#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_SPARSE_DATA
#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_RPC_WRAPPERS
#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZmeikaGameModeBase(); \
	friend struct Z_Construct_UClass_AZmeikaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Zmeika"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaGameModeBase)


#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAZmeikaGameModeBase(); \
	friend struct Z_Construct_UClass_AZmeikaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Zmeika"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaGameModeBase)


#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmeikaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmeikaGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaGameModeBase(AZmeikaGameModeBase&&); \
	NO_API AZmeikaGameModeBase(const AZmeikaGameModeBase&); \
public:


#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmeikaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaGameModeBase(AZmeikaGameModeBase&&); \
	NO_API AZmeikaGameModeBase(const AZmeikaGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmeikaGameModeBase)


#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_12_PROLOG
#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_SPARSE_DATA \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_RPC_WRAPPERS \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_INCLASS \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_SPARSE_DATA \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Zmeika_Source_Zmeika_ZmeikaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMEIKA_API UClass* StaticClass<class AZmeikaGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Zmeika_Source_Zmeika_ZmeikaGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
